# 火凤凰测试运行器
火凤凰测试运行器是基于POI和Selenium的Excel驱动的自动化测试框架。软件自带文档和丰富测试用例。
基本上，参考Hello World Demo和Test Demo的Excel测试案例，将您的测试案例置入火凤凰运行器jar所在的目录下，启动“运行”菜单项或者“运行测试案例”按钮，即可运行测试案例。测试案例的测试结果是一个在火凤凰测试运行器所在目录并以Result结尾的xls文件。
错误删除文件的缺陷已修复。
欢迎使用。

![输入图片说明](https://gitee.com/uploads/images/2017/1031/215049_7c5ca0c5_1203742.png "PheonixCyberRunner_2.png")

### 最新更新

现在 1.2.0更新ChromeDriver至最新版，使系统可以和现在的Chrome浏览器配合，大家可以使用。