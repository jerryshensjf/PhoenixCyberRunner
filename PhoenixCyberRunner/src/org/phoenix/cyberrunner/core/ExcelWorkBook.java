package org.phoenix.cyberrunner.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
/**
 * 读取excel文件中的测试数据，将数据分为用户名和密码两组
 * */
public class ExcelWorkBook{
    
   protected String username;
   protected String password;
   protected String filename;
    /**
     * 读取excel文件中的用户名列,即第一列
     * @throws IOException 
     * */
    public String readUsername(String filesrc) throws IOException{
        List<String> userList = new ArrayList<String>();
        //读取excel文件
        InputStream is = new FileInputStream(filesrc);
        POIFSFileSystem fs = new POIFSFileSystem(is);
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
        //获取文件行数
        int rows = sheet.getLastRowNum();
        //获取文件列数
        /*int cols = sheet.getRow(0).getPhysicalNumberOfCells();
        //获取第一行的数据，一般第一行为属性值，所以这里可以忽略
        String colValue1 = sheet.getRow(0).toString();
        String colValues2 = sheet.getRow(1).toString();*/
        //取出第一列的用户名，去除掉第一行中的标题
        username = sheet.getRow(1).getCell(0).toString();
        System.out.println(username);
        is.close();
        wb.close();
        return username;
    }
    
    /**
     * 获取第二列，得到密码list
     * @throws IOException 
     * */
    public String readPassword(String filesrc) throws IOException{
        //读取excel文件
        InputStream is = new FileInputStream(filesrc);
        POIFSFileSystem fs = new POIFSFileSystem(is);
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
        //取出第二列的密码值，去掉第一行中的标题
        password = sheet.getRow(2).getCell(0).toString();
        System.out.println(password);
        is.close();
        fs.close();
        wb.close();        
        return password;
    }
    
    public List<ActionGroup> getActionGroups(String filesrc) throws Exception{
    	File f = new File(filesrc);
    	this.filename = f.getName().split("\\.")[0];
    	System.out.println("JerryDebug:getRows:fileName:"+this.filename);
    	List<ActionGroup> actionGroup = new ArrayList<ActionGroup>();
        try (HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem( new FileInputStream(f)))){
        	Iterator<Sheet> iter = wb.sheetIterator();
        	while(iter.hasNext()){
        		HSSFSheet sheet = (HSSFSheet)iter.next();
        		List<Row> rowList = getRows(sheet);
        		ActionGroup ag = parseActions(rowList);
        		ag.setSheetName(sheet.getSheetName());
        		actionGroup.add(ag);
        	}
    		wb.close();
        }
        return actionGroup;
    }
    
    public List<Row> getRows(HSSFSheet sheet) throws Exception{
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
    	int rowsHeight = sheet.getLastRowNum();
    	List<Row> result = new ArrayList<Row>();
    	//skip head row
    	for (int i=0;i<=rowsHeight;i++){
    		result.add(sheet.getRow(i));
    	}
    	return result;
    }   
    
    public void writeTestResultsToExcelFile(List<ActionGroup> actionGroups) throws Exception{
    	File f = new File(this.filename+"Result.xls");
    	if (!f.exists()) f.createNewFile();
    	else {
    		f.delete();
    		f.createNewFile();
    	}
    	System.out.println("JerryDebug:getRows:writeTestResultsToExcelFile:"+f.getName());
        try (OutputStream out = new FileOutputStream(f)){
        	HSSFWorkbook wb = new HSSFWorkbook();
        	for (ActionGroup ag:actionGroups){
		        HSSFSheet sheet = wb.createSheet(ag.getSheetName());
		        HSSFRow row0 = sheet.createRow(0);
		        setupResultRow(wb,row0,ag);
		        //HSSFRow row1 = sheet.createRow(1);
		        //setupHeadRow(row1,ag.getHeadRow());
		        List<Action> actions = ag.getActions();
		        for (int i=0;i<actions.size();i++){		        	 
	                 HSSFRow row = sheet.createRow(i + 1);
	                 HSSFCell c1 = row.createCell((short)1);
	                 HSSFCell c2 = row.createCell((short)2);
	                 HSSFCell c3 = row.createCell((short)3);
	                 HSSFCell c4 = row.createCell((short)4);
	                 HSSFCell c5 = row.createCell((short)5);
	                 HSSFCell c6 = row.createCell((short)6);
	                 HSSFCell c7 = row.createCell((short)7);
	                 c1.setCellType(CellType.STRING);
	                 c2.setCellType(CellType.STRING);
	                 c3.setCellType(CellType.STRING);
	                 c4.setCellType(CellType.STRING);
	                 c5.setCellType(CellType.STRING);
	                 c6.setCellType(CellType.STRING);
	                 c7.setCellType(CellType.STRING);
	                 
	                 c1.setCellValue(actions.get(i).getTestCaseSerial());  
	                 c2.setCellValue(actions.get(i).getTestCaseDescription());
	                 c3.setCellValue(actions.get(i).getName());	  
	                 c4.setCellValue(actions.get(i).getXpath());
	                 c5.setCellValue(actions.get(i).getValue());
	                 c6.setCellValue(actions.get(i).getIsIgnore());
	                 c7.setCellValue(actions.get(i).getResult());
		        }     
        	}
	        wb.write(out);  
	        wb.close();
	        out.close();
        }
    }
    
    public void setupResultRow(HSSFWorkbook wb,HSSFRow row0,ActionGroup ag){
    	if (ag.success()) setupSuccessResultRow(wb,row0);
    	else setupFailureResultRow(wb,row0);
    }
    
    public void setupSuccessResultRow(HSSFWorkbook wb,HSSFRow row0){
   	 	HSSFCell c1 = row0.createCell((short)1);
        HSSFCell c2 = row0.createCell((short)2);
        HSSFCell c3 = row0.createCell((short)3);
        HSSFCell c4 = row0.createCell((short)4);
        HSSFCell c5 = row0.createCell((short)5);
        HSSFCell c6 = row0.createCell((short)6);
        HSSFCell c7 = row0.createCell((short)7);
        c1.setCellType(CellType.STRING);
        c2.setCellType(CellType.STRING);
        c3.setCellType(CellType.STRING);
        c4.setCellType(CellType.STRING);
        c5.setCellType(CellType.STRING);
        c6.setCellType(CellType.STRING);
        c7.setCellType(CellType.STRING);
        HSSFCellStyle style = wb.createCellStyle();
        
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREEN.index);
        c1.setCellStyle(style);  //cell 是 HSSFCell 对象
        c2.setCellStyle(style); 
        c3.setCellStyle(style); 
        c4.setCellStyle(style); 
        c5.setCellStyle(style); 
        c6.setCellStyle(style); 
        c7.setCellStyle(style);       

        c4.setCellValue("Success");
   }
    
    public void setupFailureResultRow(HSSFWorkbook wb,HSSFRow row0){
   	 	HSSFCell c1 = row0.createCell((short)1);
        HSSFCell c2 = row0.createCell((short)2);
        HSSFCell c3 = row0.createCell((short)3);
        HSSFCell c4 = row0.createCell((short)4);
        HSSFCell c5 = row0.createCell((short)5);
        HSSFCell c6 = row0.createCell((short)6);
        HSSFCell c7 = row0.createCell((short)7);
        c1.setCellType(CellType.STRING);
        c2.setCellType(CellType.STRING);
        c3.setCellType(CellType.STRING);
        c4.setCellType(CellType.STRING);
        c5.setCellType(CellType.STRING);
        c6.setCellType(CellType.STRING);
        c7.setCellType(CellType.STRING);
        HSSFCellStyle style = wb.createCellStyle();
        
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.RED.index);
        c1.setCellStyle(style);  //cell 是 HSSFCell 对象
        c2.setCellStyle(style); 
        c3.setCellStyle(style); 
        c4.setCellStyle(style); 
        c5.setCellStyle(style); 
        c6.setCellStyle(style); 
        c7.setCellStyle(style);       

        c4.setCellValue("Failure");
   }
    
    public void setupHeadRow(HSSFRow row1,HeadRow headRow){
    	 HSSFCell c1 = row1.createCell((short)1);
         HSSFCell c2 = row1.createCell((short)2);
         HSSFCell c3 = row1.createCell((short)3);
         HSSFCell c4 = row1.createCell((short)4);
         HSSFCell c5 = row1.createCell((short)5);
         HSSFCell c6 = row1.createCell((short)6);
         HSSFCell c7 = row1.createCell((short)7);
         c1.setCellType(CellType.STRING);
         c2.setCellType(CellType.STRING);
         c3.setCellType(CellType.STRING);
         c4.setCellType(CellType.STRING);
         c5.setCellType(CellType.STRING);
         c6.setCellType(CellType.STRING);
         c7.setCellType(CellType.STRING);
         
         c1.setCellValue(headRow.getHeaderArr()[0]);  
         c2.setCellValue(headRow.getHeaderArr()[1]);
         c3.setCellValue(headRow.getHeaderArr()[2]);	  
         c4.setCellValue(headRow.getHeaderArr()[3]);
         c5.setCellValue(headRow.getHeaderArr()[4]);
         c6.setCellValue(headRow.getHeaderArr()[5]);
         c7.setCellValue(headRow.getHeaderArr()[6]);
    }
    
     public ActionGroup parseActions(List<Row> rows){
    	 List<Action> result = new ArrayList<Action>();
    	 for (int i=0;i<rows.size();i++){
    		 if (i >= findHeadRowIndex(rows)){
	    		 Action ac = mapRowToAction(rows.get(i));
	    		 result.add(ac);
    		 }
    	 }
    	 ActionGroup actionGroup = new ActionGroup();
    	 actionGroup.setActions(result);
    	 return actionGroup;
     }
     
     public Integer findHeadRowIndex(List<Row> rows){
    	 for (int i=0;i<rows.size();i++){
    		 if (equalsHeadRow(rows.get(i))) return i;
    	 }
    	 return null;
     }
     
     public boolean equalsHeadRow(Row row){
    	 boolean result = true;
    	 for (int i=1;i< row.getLastCellNum()&&i-1<HeadRow.getHeaderArr().length;i++){
    		 if (row!=null&&row.getCell(i)!=null&&!getCellStringValue(row.getCell(i)).equals(HeadRow.getHeaderArr()[i-1])) result = false;
    	 }
    	 return result;
     }
     
     public String getCellStringValue(Cell c){
    	 if (c.getCellType()==HSSFCell.CELL_TYPE_STRING) return c.getStringCellValue();
    	 else if (c.getCellType()==HSSFCell.CELL_TYPE_NUMERIC) return ""+c.getNumericCellValue();
    	 else return "";
     }
     
     public Action mapRowToAction(Row row){
    	 Action ac = new Action();
    	 String serial = "";
    	 String desc ="";
    	 String action="";
    	 String xpath="";
    	 String value="";
    	 String ignore="";
    	 
    	 if(row.getCell(1)!=null) serial = row.getCell(1).toString();
    	 if(row.getCell(2)!=null) desc = row.getCell(2).toString();
    	 if(row.getCell(3)!=null) action = row.getCell(3).toString();
    	 if(row.getCell(4)!=null) xpath = row.getCell(4).toString();
    	 if(row.getCell(5)!=null) value = row.getCell(5).toString();
    	 if(row.getCell(6)!=null) ignore = row.getCell(6).toString();
    	 ac.setTestCaseSerial(serial);
    	 ac.setTestCaseDescription(desc);
    	 ac.setName(action);
    	 ac.setXpath(xpath);
    	 ac.setValue(value);
    	 ac.setIsIgnore(ignore);
    	 return ac;    	 
     }

}

