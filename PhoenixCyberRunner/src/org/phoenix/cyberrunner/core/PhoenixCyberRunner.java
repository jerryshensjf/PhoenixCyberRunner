package org.phoenix.cyberrunner.core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.junit.JUnit4TestRunner;


public class PhoenixCyberRunner{      
    public void doGet(WebDriver driver,String value){
    	Navigation navigate = driver.navigate();
        navigate.to(value);
    }
    
    public void doInput(WebDriver driver,String xpath, String value){
    	WebDriverWait myWait = new WebDriverWait(driver,1);
    	WebElement field = myWait.until(new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xpath));
			}});   
    	field.sendKeys(value);
    }
    
    public void doSelect(WebDriver driver,String xpath, String value){
    	WebDriverWait myWait = new WebDriverWait(driver,1);
    	WebElement parentfield = myWait.until(new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xpath));
			}});
    	List<WebElement> fields = parentfield.findElements(By.tagName("option"));
    	for (WebElement we:fields){
    		if (value.equals(we.getText())) we.click();
    	}
    }
    
    public void doClickButton(WebDriver driver,String xpath,Integer pos) throws Exception{ 
    	List<WebElement> btns = driver.findElements(By.xpath(xpath));
    	if (btns != null && btns.size()>=1&&pos!=null&&pos>=0&&pos<btns.size()){
    			if (btns.get(pos).isEnabled()) {
    				btns.get(pos).click();
    			}
    			else {
    				for (int i=0;i<btns.size();i++){
    					if (btns.get(i).isEnabled()){
    						btns.get(i).click();
    						return;
    					}
    				}
    			}
    	} else if (btns != null && btns.size()>=1&&pos==null){
    		btns.get(0).click();
    		return;
    	}else {
    		throw new Exception("Button  did not exist");
    	}
    }
    
    public void doWait(WebDriver driver,String xpath,String value) throws Exception{
    	WebDriverWait myWait = new WebDriverWait(driver,(int)Double.parseDouble(value));
    	myWait.until(new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xpath));
			}});    	
    }
    
    public void doIframeInput(WebDriver driver,String xpath, String value){
    	WebElement frame=driver.findElement(By.xpath( ".//iframe" ));
    	driver.switchTo().frame(frame);
		WebElement field = driver.findElement(By.xpath(xpath));
		if (field != null){
			field.clear();
			field.sendKeys(value);
		}
    }
    
    public void doIframeClick(WebDriver driver,String xpath){
    	WebElement frame=driver.findElement(By.xpath( ".//iframe" ));
    	driver.switchTo().frame(frame);
		WebElement btn = driver.findElement(By.xpath(xpath));
		if (btn != null){
			btn.click();
		}
    }
    
    public void doIframeWaitAndSwitch(WebDriver driver,String value){
    	WebDriverWait myWait = new WebDriverWait(driver,(int)Double.parseDouble(value));
    	WebElement frame = myWait.until(new ExpectedCondition<WebElement>(){
			@Override
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(".//iframe"));
			}});
    	driver.switchTo().frame(frame);
    }
       
    public void doValidateValue(WebDriver driver,Action action,String xpath,String value){
    	WebElement field = driver.findElement(By.xpath(xpath));
    	String fval = field.getAttribute("value");
    	System.out.println("JerryDebug:field:"+fval);
    	if (field != null && fval!=null && fval.equals(value)) action.setResult("success");
    	else if (field != null && fval!=null && !"".equals(fval)&& isNumeric(fval) && Double.parseDouble(fval)-Double.parseDouble(value)<=0.000001d)  action.setResult("success");
    	else action.setResult("failure");
    	System.out.println("JerryDebug:doValidateValue:"+action.getResult());
    } 
    
    public void doValidateText(WebDriver driver,Action action,String xpath,String value){
    	WebElement field = driver.findElement(By.xpath(xpath));
    	String fval = field.getText();
    	System.out.println("JerryDebug:field:"+fval);
    	if (field != null && fval!=null && fval.equals(value)) action.setResult("success");
    	else if (field != null && fval!=null && !"".equals(fval)&& isNumeric(fval) && Double.parseDouble(fval)-Double.parseDouble(value)<=0.000001d)  action.setResult("success");
    	else action.setResult("failure");
    	System.out.println("JerryDebug:doValidateText:"+action.getResult());
    } 
    
	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
    
    public void doValidateTitile(WebDriver driver,Action action,String title){
    	if (driver != null && driver.getTitle()!=null && driver.getTitle().equals(title)) action.setResult("success");
    	else action.setResult("failure");
    	System.out.println("JerryDebug:doValidateTitile:"+action.getResult());
    } 
    
    public void doValidatePageContains(WebDriver driver,Action action,String content){
    	if (driver != null && driver.getPageSource()!=null && driver.getPageSource().contains(content)) action.setResult("success");
    	else action.setResult("failure");
    	System.out.println("JerryDebug:doValidatePageContains:"+action.getResult());
    } 
    
    public List<ActionGroup>  parseExcelTestCases(ExcelWorkBook excelBook,String pathName) throws Exception{
    	 return excelBook.getActionGroups(pathName);
    }
    
    public void doActions(WebDriver driver,ActionGroup actions) throws Exception{
    	for (Action ac:actions.getActions()){    		
    		if (!(ac.getIsIgnore()!=null&&ac.getIsIgnore().equals("ignore"))){
    			if ("get".equals(ac.getName())) doGet(driver,ac.getValue());
    			else if ("input".equals(ac.getName())) doInput(driver,ac.getXpath(),ac.getValue());
    			else if ("select".equals(ac.getName())) doSelect(driver,ac.getXpath(),ac.getValue());
	    		else if ("click".equals(ac.getName())) {
	    			Integer pos = 0;
	    			String posStr = ac.getValue();
	    			if (posStr!=null && !"".equals(posStr)) pos = Integer.parseInt(posStr);
	    			doClickButton(driver,ac.getXpath(),pos);
	    		}
	    		else if ("wait".equals(ac.getName())) doWait(driver,ac.getXpath(),ac.getValue());
	    		else if ("iframeinput".equals(ac.getName())) doIframeInput(driver,ac.getXpath(),ac.getValue());
	    		else if ("iframeclick".equals(ac.getName())) doIframeClick(driver,ac.getXpath());
	    		else if ("iframewaitandswitch".equals(ac.getName())) doIframeWaitAndSwitch(driver,ac.getValue());
	    		else if ("validateValue".equals(ac.getName())) doValidateValue(driver,ac,ac.getXpath(),ac.getValue());
	    		else if ("validateText".equals(ac.getName())) doValidateText(driver,ac,ac.getXpath(),ac.getValue());
	    		else if ("validateTitle".equals(ac.getName())) doValidateTitile(driver,ac,ac.getValue());
	    		else if ("validatePageContains".equals(ac.getName())) doValidatePageContains(driver,ac,ac.getValue());
    		}
    	}
    }
    
	@Test
	public void testExcelTestCases() {
		ExcelWorkBook excelBook = null;
		WebDriver driver = null;
		Navigation navigate = null;
		try {
			String chromeDriverLocation = "";

			excelBook = new ExcelWorkBook();
			File dir = new File(".");
			String dirFolder = dir.getAbsolutePath().substring(0,dir.getAbsolutePath().length()-1);
			String[] fileNames = dir.list();
			List<String> excelTestCases = new ArrayList<String>();
			for (String fileName : fileNames) {
				if (!"".equals(fileName) && fileName.endsWith(".xls") && !fileName.endsWith("Result.xls")) {
					excelTestCases.add(fileName);
				}
				if (!"".equals(fileName) && fileName.equals("config.properties")) {
					Properties prop = new Properties();
					String configPath = (dirFolder+ "config.properties").replaceAll("\\\\", "/");
					System.out.println("JerryDebug:configPath:"+configPath);
					try (InputStream in = new BufferedInputStream(new FileInputStream(configPath))) {
						try {
							ResourceBundle resourceBundle = new PropertyResourceBundle(in);
							chromeDriverLocation = resourceBundle.getString("chromeDriverLocation");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
//				if (!"".equals(fileName) && fileName.equals("chromedriver.exe")) {
//					chromeDriverLocation = dirFolder + "C:/JerryWorkspace/PhoenixCyberRunner/src/resource/chromedriver.exe";
//					System.out.println("JerryDebug:chromeDriverLocation:" + chromeDriverLocation);
//				}
			}
			//System.setProperty("webdriver.chrome.driver", dirFolder+"/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dirFolder+"/chromedriver");
			//ChromeOptions  options = new ChromeOptions();
			//options.addArguments("--whitelisted-ips=\"\"");
			//ChromeOptions options = new ChromeOptions();
			//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			//capabilities.setCapability("chrome.switches", Arrays.asList("--start-maximized"));
			//options.addArguments("--test-type", "--whitelisted-ips","");
			driver = new ChromeDriver();
		
			navigate = driver.navigate();
			// navigate.to(url);
			for (String excelName : excelTestCases) {
				List<ActionGroup> actionGroups = parseExcelTestCases(excelBook, excelName);
				for (ActionGroup ag : actionGroups)
					doActions(driver, ag);
				excelBook.writeTestResultsToExcelFile(actionGroups);
			}
			driver.close();
		} catch (Exception e) {
			driver.close();
			e.printStackTrace();
		} finally {
			driver.close();
		}
	}
    
    public static void main(String [] args){
    	JUnit4TestRunner runner = new JUnit4TestRunner();
    	runner.run(PhoenixCyberRunner.class, "testExcelTestCases");
    }

}

