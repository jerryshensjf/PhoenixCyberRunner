package org.phoenix.cyberrunner.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.phoenix.cyberrunner.core.PhoenixCyberRunner;

@SuppressWarnings("serial")
class AboutFrame extends JFrame implements MouseListener {
	
	private JPanel aboutPane;

	private JLabel msg;

	private JLabel msg1;

	private JLabel msg2;
	
	private JLabel msg3;

	private JButton exit;

	public AboutFrame(String strName) {
		super(strName);
		try {
			String windows="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
			//UIManager.setLookAndFeel(windows);
			setSize(360, 400);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
			aboutPane = new JPanel();
			msg = new JLabel("        火凤凰测试运行器                      ");
			msg1 = new JLabel("               Enjoy!            ");
			msg2 = new JLabel("      版本： 1.0                  ");
			msg3 = new JLabel("      版本发布时间: 2017-11-11    ");
			exit = new JButton("退出");
			exit.addMouseListener(this);
			aboutPane.add(msg);
			aboutPane.add(msg1);
			aboutPane.add(msg2);
			aboutPane.add(msg3);
			aboutPane.add(exit);
	
			setContentPane(aboutPane);
			setLocation(500, 440);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	// the event handle to deal with the mouse click
	public void mouseClicked(MouseEvent e) {
		this.setVisible(false);
	}

	public void mousePressed(MouseEvent e) {
		//System.out.println("Jerry Press");

	}

	public void mouseReleased(MouseEvent e) {
		//System.out.println("Jerry Release");
	}

	public void mouseExited(MouseEvent e) {
		//System.out.println("Jerry Exited");

	}

	public void mouseEntered(MouseEvent e) {
		//System.out.println("Jerry Entered");

	}

	public static void main(String[] args) {
		AboutFrame about = new AboutFrame("关于火凤凰");
		about.setVisible(true);
	}
}
