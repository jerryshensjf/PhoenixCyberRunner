package org.phoenix.cyberrunner.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.phoenix.cyberrunner.core.PhoenixCyberRunner;
import org.phoenix.cyberrunner.core.Resource;


@SuppressWarnings("serial")
public class PhoenixCyberRunnerUI extends JFrame implements MouseListener, ActionListener {
	private PhoenixCyberRunner runner = new PhoenixCyberRunner();
	
	public static void main(String [] args) {
		PhoenixCyberRunnerUI runnerui = new PhoenixCyberRunnerUI();
		runnerui.setVisible(true);
	}
	
	private AboutFrame about;

	private JMenuBar mb;
	
	private JMenu mRun;
	
	private JMenu mDown;

	private JMenu mHelp;

	private JMenuItem miAbout;

	private JMenuItem miHelp;

	private JMenuItem miRun;
	
	private JMenuItem miHelloWorld;
	
	private JMenuItem miTestDemo;

	private JMenuItem miExit;
	
	private JMenuItem miWar;
	
	private JMenuItem miSource;
	
	private JMenuItem miSql;
	
	private JMenuItem miSqlTest;
	
	private JMenuItem miManual;
	
	private JPanel pane;
	
	private GridBagLayout gridbag;
	
	private GridBagConstraints constraints;
	
	private GridBagConstraints constraints3;
	
	private JLabel lblExcelSheet;
	
	private JTextField txtExcelSheet;
	
	private JButton btnUpload;
	
	private JButton btnRun;

	// Constructor of the game
	public PhoenixCyberRunnerUI() {
		super("火凤凰测试运行器");
		try {
			String windows="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
			//UIManager.setLookAndFeel(windows);
			 
			setSize(400, 600);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
			
			gridbag = new GridBagLayout();
			constraints = new GridBagConstraints();
			//constraints.fill = GridBagConstraints.BOTH;
			constraints.anchor = GridBagConstraints.CENTER;
			constraints.ipady = 10;
			constraints.insets = new Insets(0,0,10,10);
			constraints.gridheight = 7;
			constraints.gridwidth = 0;			

			pane = new JPanel();	
			
			lblExcelSheet = new JLabel("测试案例:");
			lblExcelSheet.setSize(300,120);
			
			txtExcelSheet = new JTextField("                         ");
			txtExcelSheet.setSize(500, 120);
			txtExcelSheet.addActionListener(this);
			
			btnUpload = new JButton(" 上传测试案例 ");
			btnUpload.setSize(300, 100);
			btnUpload.addActionListener(this);
			
			btnRun = new JButton(" 运行测试案例 ");
			btnRun.setSize(300, 100);
			btnRun.addActionListener(this);
			
			gridbag.setConstraints(lblExcelSheet, constraints);
			gridbag.setConstraints(txtExcelSheet, constraints);
			gridbag.setConstraints(btnUpload, constraints);
			gridbag.setConstraints(btnRun, constraints);
			
			pane.setLayout(gridbag);
			
			pane.add(lblExcelSheet);
			pane.add(txtExcelSheet);
			pane.add(btnUpload);
			pane.add(btnRun);
	
	
			// Begin Menu Set
			mb = new JMenuBar();
			mRun = new JMenu("运行");
			miRun = new JMenuItem("运行");
			miRun.addActionListener(this);
			
			miHelloWorld = new JMenuItem("Hello World Demo");
			miHelloWorld.addActionListener(this);
			
			miTestDemo = new JMenuItem("Test Demo");
			miTestDemo.addActionListener(this);
			
			miExit = new JMenuItem("退出");
			miExit.addActionListener(this);

			mb.add(mRun);
			mRun.add(miRun);
			mRun.add(miHelloWorld);
			mRun.add(miTestDemo);
			
			mRun.addSeparator();
			mRun.add(miExit);
			
			
			mDown = new JMenu("下载");
			miWar = new JMenuItem("测试示例下载");
			miWar.addActionListener(this);
			
			miSource = new JMenuItem("测试示例源码下载");
			miSource.addActionListener(this);
			
			miSql = new JMenuItem("测试示例Sql下载");
			miSql.addActionListener(this);
			
			miSqlTest = new JMenuItem("测试示例测试库Sql下载");
			miSqlTest.addActionListener(this);
			
			miManual = new JMenuItem("用户完全手册下载");
			miManual.addActionListener(this);

			mb.add(mDown);
			mDown.add(miWar);
			mDown.add(miSource);
			mDown.add(miSql);
			mDown.add(miSqlTest);
			mDown.addSeparator();
			mDown.add(miManual);
			mb.add(mDown);
	
			mHelp = new JMenu("帮助");
			miHelp = new JMenuItem("帮助...");
			miHelp.addActionListener(this);
			mHelp.add(miHelp);
			
			miAbout = new JMenuItem("关于...");
			mHelp.add(miAbout);
			miAbout.addActionListener(this);
			mb.add(mHelp);
			this.setJMenuBar(mb);
			// end of Menu Set
	
			setContentPane(pane);
			setLocation(400, 200);
			setVisible(true);

			// About Frame
			about = new AboutFrame("关于火凤凰");
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == miAbout) {
				about.setVisible(true);
			}
			
			if (e.getSource() == miRun) {
				runner.main(null);
				JOptionPane.showMessageDialog(null, "测试已运行结束，请检查运行文件夹中的结果Excel文件！", "运行结束", JOptionPane.PLAIN_MESSAGE);
			}
			
			if (e.getSource() == miHelloWorld) {
				cleanWorkFolder();
				downloadRes("/resource/chromedriver.exe","chromedriver.exe");
				downloadRes("/resource/chromedriver","chromedriver");
				downloadRes("/resource/HelloWorld.xls","HelloWorld.xls");
				txtExcelSheet.setText("HelloWorld.xls");
				runner.main(null);
				JOptionPane.showMessageDialog(null, "测试已运行结束，请检查运行文件夹中的结果Excel文件！", "运行结束", JOptionPane.PLAIN_MESSAGE);
			}
			
			if (e.getSource() == miTestDemo) {
				cleanWorkFolder();
				downloadRes("/resource/chromedriver.exe","chromedriver.exe");
				downloadRes("/resource/chromedriver","chromedriver");
				downloadRes("/resource/TestDemo.war","TestDemo.war");
				downloadRes("/resource/TestDemo_20171029.zip","TestDemo_20171029.zip");
				downloadRes("/resource/TestDemo.xls","TestDemo.xls");
				txtExcelSheet.setText("TestDemo.xls");
				runner.main(null);
				JOptionPane.showMessageDialog(null, "测试已运行结束，请检查运行文件夹中的结果Excel文件！", "运行结束", JOptionPane.PLAIN_MESSAGE);
			}
			
			if (e.getSource() == miWar){
				downloadRes("/resource/TestDemo.war","TestDemo.war");  
			}
			if (e.getSource() == miSource){
				downloadRes("/resource/TestDemo_20171029.zip","TestDemo_20171029.zip");  
			}
			if (e.getSource() == miSql){
				downloadRes("/resource/testdemo.sql","testdemo.sql");  
			}
			if (e.getSource() == miSqlTest){
				downloadRes("/resource/testdemo_test.sql","testdemo_test.sql");  
			}
			if (e.getSource() == miManual){
				downloadRes("/resource/火凤凰测试运行器用户完全手册.docx","火凤凰测试运行器用户完全手册.docx");  
			}
			
			if (e.getSource() == btnRun) {
				runner.main(null);
				JOptionPane.showMessageDialog(null, "测试已运行结束，请检查运行文件夹中的结果Excel文件！", "运行结束", JOptionPane.PLAIN_MESSAGE);
			}
			
			if (e.getSource() == btnUpload) {
				JFileChooser fd = new JFileChooser();
				fd.setDialogTitle("打开测试案例Excel文件");
				XlsFileFilter xfilter = new XlsFileFilter("xls");
				fd.addChoosableFileFilter(xfilter);
				fd.setFileFilter(xfilter);
				//fd.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int result = fd.showOpenDialog(this);
				
				if (result == JFileChooser.APPROVE_OPTION) {
					File f = fd.getSelectedFile(); 
					if(f != null){
						String filename= f.getName();
						String selectedFolder = f.getAbsolutePath().substring(0,f.getAbsolutePath().length()-1);
						
						File dir = new File(".");
						String dirFolder = dir.getAbsolutePath().substring(0,dir.getAbsolutePath().length()-1);
						File destFolder = new File(dirFolder);
						File destFile =  new File(dirFolder + "/" + filename);
						if (!selectedFolder.equals(destFolder)) {
							File[] destFiles = destFolder.listFiles(xfilter);
							for (File df:destFiles) df.delete();							
							copyFile(f,destFile);	
						}					
						txtExcelSheet.setText(filename);
						JOptionPane.showMessageDialog(null, "测试案例"+filename+"已上传，可以运行！", "上传成功", JOptionPane.PLAIN_MESSAGE);
					}  
				}
			}
			
		} catch (Exception ie) {
			ie.printStackTrace();;
		}
	}
	
	public void cleanWorkFolder(){
		File dir = new File(".");
		String dirFolder = dir.getAbsolutePath().substring(0,dir.getAbsolutePath().length()-1);
		File [] files = dir.listFiles();
		for (File f:files){
			String fname = f.getName();
			String [] fnames = fname.split(".");
			String fext = "";
			if (fnames.length >= 1)  fext = fnames[fnames.length-1];
			if (fext!=null && !fext.equals("") && fext.equals("xls")&&fname!=null&&!fname.endsWith("Result.xls")){
				f.delete();
			}		
		}
			
	}

	// the mouse events listener methods
	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void runTest(){
		this.runner.testExcelTestCases();
	}
	
	protected void downloadRes(String filePath, String newFileName)throws Exception{
		new Resource().downloadRes(filePath,newFileName);
	}
	
	 // 复制文件
    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            // 新建文件输入流并对它进行缓冲
            inBuff = new BufferedInputStream(new FileInputStream(sourceFile));

            // 新建文件输出流并对它进行缓冲
            outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));

            // 缓冲数组
            byte[] b = new byte[1024 * 5];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            // 刷新此缓冲的输出流
            outBuff.flush();
        } finally {
            // 关闭流
            if (inBuff != null)
                inBuff.close();
            if (outBuff != null)
                outBuff.close();
        }
    }
}

class XlsFileFilter extends FileFilter implements FilenameFilter {
	String ext;

	public XlsFileFilter(String ext) {
		this.ext = ext;
	}

	/* 在accept()方法中,当程序所抓到的是一个目录而不是文件时,我们返回true值,表示将此目录显示出来. */
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		String fileName = file.getName();
		int index = fileName.lastIndexOf('.');
		if (index > 0 && index < fileName.length() - 1) {
			// 表示文件名称不为".xxx"现"xxx."之类型
			String extension = fileName.substring(index + 1).toLowerCase();
			String firstFileName =  fileName.substring(0,index).toLowerCase();
			// 若所抓到的文件扩展名等于我们所设置要显示的扩展名(即变量ext值),则返回true,表示将此文件显示出来,否则返回
			// true.
			if (extension.equals(ext)&&!firstFileName.endsWith("result"))
				return true;
		}
		return false;
	}

	// 实现getDescription()方法,返回描述文件的说明字符串!!!
	public String getDescription() {
		if (ext.equals("xls"))
			return "Excel files(*.xls)";
		return "";
	}

	@Override
	public boolean accept(File dir, String name) {
		return this.accept(new File(dir.getAbsolutePath()+"/"+name));
	}
}




